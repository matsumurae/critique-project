# 0.1.4 (07-06-2016)
This fixes should have been uploaded in 07/06, but I forget to upload it.
- Added `shots` collection in mongo
- Added `shots` list in mongo collection (shots)
- Display dynamically shots from database
- Shot detail doesn't display shot details actually. Upload in next version.
- Fixed `key` problems with React (when using map).
- Added Shots publication for display shot list and shot detail.
- Add a container for subscribe to mongo collection and display in react component.
- Add shot detail route.
- Create individual `tab` component and generate dynamically depending on tabs array (in `tabs` component).

# 0.1.3 (06-06-2016)
- Added shot list (static images).
- Added tabs.
- Added Image and Tabs as shared components.
- Register only requires one password.
- When user are logged in, can view in `profile` his name (if has) and in `account` his email.

# 0.1.2 (05-06-2016)
- Added svg loader.
- Navigation bar now display active link
- Added `react-dd-menu` and `alanning:roles`.
- Updated css.
- Added 404 page styled.
- Updated README
- Added README to client, lib and server folder. Should add explanations.

# 0.1.1 (03-06-2016)
- Added navbar and footer components from prototype
- Fixed login problem: when user are logged in and refresh the page with F5, app crashes.
- Added `classnames` as dependency
- Created `shared` folder for contain shared components/actions/etc.
- Avatar, logo and searchbar are now shared component
- Upload `fonts` for material-icons

# 0.1.0 (03-06-2016)
First upload:
- Removed bootstrap styles
- Upload `main.css` styles from prototype (should add `scss` to each component)
