### Public
The `public` directory is designed to hold static assets like images, documents, and other files intended for access by the public. 

> When referencing these assets, do not include `public/` in the url. Eg. `public/bg.png` as `<img src="bg.png" />`
