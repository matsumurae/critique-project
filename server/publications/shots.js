import {Shots} from '/lib/collections';
import {Meteor} from 'meteor/meteor';
import {check} from 'meteor/check';

export default function () {
  Meteor.publish('shots.list', function () {
    const selector = {};
    return Shots.find(selector);
  });

  Meteor.publish('shot.single', function (shotId) {
    check(shotId, String);
    const selector = {_id: shotId};
    return Shots.find(selector);
  });
}
