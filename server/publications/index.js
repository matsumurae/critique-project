import posts from './posts';
import shots from './shots';
import UserCurrent from './UserCurrent';

export default function () {
  posts();
  shots();
  UserCurrent();
}
