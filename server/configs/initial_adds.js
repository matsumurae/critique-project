import {Posts, Shots} from '/lib/collections';

export default function () {
  let imagesArray = [
    {src: 'https://static.pexels.com/photos/25352/pexels-photo-25352-large.jpg', alt: 'Landscape'},
    {src: 'https://static.pexels.com/photos/4477/nature-forest-trees-path-large.jpg', alt: 'Forest'},
    {src: 'https://static.pexels.com/photos/60239/pexels-photo-60239-large.jpeg', alt: 'Misty'},
    {src: 'https://pixabay.com/static/uploads/photo/2016/01/02/00/40/wheat-1117267_960_720.jpg', alt: 'Wheat'},
    {src: 'https://pixabay.com/static/uploads/photo/2016/05/01/20/43/forget-me-not-1365857_960_720.jpg', alt: 'Flower'},
    {src: 'https://pixabay.com/static/uploads/photo/2015/12/12/00/29/jurassic-coast-1089035_960_720.jpg', alt: 'Sunset'},
    {src: 'https://pixabay.com/static/uploads/photo/2016/03/16/11/38/animal-1260334_960_720.jpg', alt: 'Duck'},
    {src: 'https://pixabay.com/static/uploads/photo/2016/05/25/18/02/autumn-leave-1415541_960_720.jpg', alt: 'autumn'},
    {src: 'https://pixabay.com/static/uploads/photo/2016/05/24/11/54/lake-1412216_960_720.jpg', alt: 'Lake'},
    {src: 'https://pixabay.com/static/uploads/photo/2016/05/19/18/31/hyacinth-1403653_960_720.jpg', alt: 'Violet'},
    {src: 'https://pixabay.com/static/uploads/photo/2016/05/07/19/21/baguette-1378049_960_720.jpg', alt: 'Bread'},
    {src: 'https://pixabay.com/static/uploads/photo/2016/05/02/13/17/dear-1367217_960_720.jpg', alt: 'Deer'}
  ];

  if (!Posts.findOne()) {
    for (let lc = 1; lc <= 5; lc++) {
      const title = `This is the post title: ${lc}`;
      const content = `Post ${lc}'s content is great!`;
      Posts.insert({title, content});
    }
  }

  if (!Shots.findOne()) {
    imagesArray.forEach(function (image) {
      const src = image.src;
      const alt = image.alt;
      Shots.insert({src, alt});
    });
  }
}
