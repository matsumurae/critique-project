# Critique
Critique project developed using Meteor + React.

## Why this architecture?
We use Mantra as the application architecture. Is designed specifically for Meteor apps. Follows the MVC architecture:
![Mantra](architecture.jpeg)
As we see in the image, this are the main parts:
- `React UI components` acts as **Views**. This contains the user interface parts without any logic.
- `React Containers` acts as **Controllers**. Is used for have **dumb components**. With this, we avoid insert data logic into UI.
- `Actions` acts as **Models**. This contains the business logic.
- `State managers` acts a **Local/Remote states**. Here we can manage the local states (like errors, validation messages...) or remote (when is fetched from the server).

And why I choose?
- Is easy to follow when start developing, due to it's distribution in directories.
- Brings a lot of advantages like flexibility and maintainability.

## Software development process
Used Kanban as first with a pinch of scrumb for some things:
- We manage the tasks in a board, so it's easy to control the maximum of opened tasks at a time.
- Tasks are user stories. User stories is the best manner for know the app requirements and estimate time.
- We can manage tasks by type/tag. With this, we can identify rapidly which type of task we need to do without read the description.
- Using some addons for trello (like trello card numbers) for identify fastly which number of task are you doing, for annotate in your git commits. Also, can know how many tasks you have in that card.

## Structure
We find this folders:
- `.meteor` includes all meteor necessary data. Here we can know which version of Meteor we have installed and which packages(with versions).
- `client` includes all the code that will be used in the client. This includes the UI, CSS...
- `server` includes all the code that will be used in the server. This includes methods, publications and configs.
- `lib` includes all the created mongo collections.
- `public` includes all the static data like images, fonts...
- `.storybook` is a special folder. Created by the `react storybook` package, is used to develop and design React UI components without running our app.

**`client`, `server`, `public` and `lib` have inside a README file explaining what you will find there. Within each subdirectory you can find a README of what you have there.**

## Used packages
### NPM
- `eslint` with `eslint-plugin-react`.
- `react` with `react-dom`.
- `Enzyme` with `react-addons-test-utils` for testing react components.
- `bulma-react` for substitute bootstrap.
- `classnames` for join classNames together.
- `lodash` for fast development.
- `moment` for parse, validate, manipulate and format dates.
- `react-dd-menu` for dropdown menu with react.
- `react-select` for an amazing select customizable.

### Atmosphere
- `spiderable` for make app crawlable to web spiders.
- `flow-router` for routing.
- `scss` for compile sass/scss.
- `mongol` for edit mongo in UI.
- `ddp-rate-limiter` for add rate limits ddp methods and subcriptions.
- `mocha` for testing.
- `validated-method` for wrapper of Meteor.methods.
- `validation-error` for standard validation error.
