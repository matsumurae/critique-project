import React from 'react';

export default class extends React.Component {
  render() {
    return (
      <div className="nav-search">
        <span className="material-icons font-24">search</span>
        <input type="search" className="searchbar" placeholder="Search..." />
      </div>
    );
  }
}
