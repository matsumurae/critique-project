import React from 'react';

class Tab extends React.Component {

  isActive() {
    return FlowRouter.current().path === this.props.href;
  }

  linkTab() {
    let style = '';
    let props = this.props;
    if (this.isActive()) {
      style = 'is-active';
    }

    return (
      <li className={style}>
        <a className="is-tab" href={props.href}>{props.name}</a>
      </li>
    );
  }

  render() {
    return this.linkTab();
  }
}

export default Tab;
