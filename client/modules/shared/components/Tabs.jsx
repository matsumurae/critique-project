import React from 'react';

import Tab from './Tab.jsx';

class Tabs extends React.Component {

  renderTabs(tabs) {
    if (tabs.length > 0) {
      return tabs.map((tab, index) => (
          <Tab key={index} href={tab.href} name={tab.name} />
      ));
    }
  }

  render() {
    let tabsArray = [
      {name: 'Recent', href: '?recent'},
      {name: 'Most popular', href: '?most-popular'},
      {name: 'Most reviewed', href: '?most-reviewed'}
    ];

    const tabs = this.renderTabs(tabsArray);

    return (
      <nav className="level tabs is-medium m-0">
        <div className="level-left">
          <ul className="level-item m-0">
            { tabs }
          </ul>
        </div>
        <div className="level-right">
          <ul className="level-item m-0">
            <li>
               <a href="#" className="button is-medium is-accent-light">Filters</a>
            </li>
          </ul>
        </div>
      </nav>
    );
  }
}

export default Tabs;
