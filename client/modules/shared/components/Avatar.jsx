import React from 'react';
import classNames from 'classnames';

class Avatar extends React.Component {

  avatarRender() {
    let style = 'avatar avatar-inline margin-left';
    let props = this.props;
    style += ` ${classNames(props)}` + ` ${props.className}`;
    return (
      <div className={style}>
        <img src={props.imgSrc} alt={props.imgAlt} />
      </div>
    );
  }

  render() {
    return this.avatarRender();
  }

}

Avatar.propTypes = {
  style: React.PropTypes.string,
  imgSrc: React.PropTypes.string,
  imgAlt: React.PropTypes.string
};

Avatar.defaultProps = {
  style: '',
  imgSrc: 'http://placehold.it/100x100',
  imgAlt: 'avatar'
};

export default Avatar;
