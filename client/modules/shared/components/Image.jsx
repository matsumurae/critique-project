import React from 'react';

class Image extends React.Component {

  render() {
    let props = this.props;
    return (
      <figure className="gallery-image">
        <a href={props.href}>
          <img src={props.imgSrc} alt={props.imgAlt} />
        </a>
      </figure>
    );
  }

}

Image.propTypes = {
  imgSrc: React.PropTypes.string.isRequired,
  imgAlt: React.PropTypes.string
};

Image.defaultProps = {
  imgAlt: ''
};

export default Image;
