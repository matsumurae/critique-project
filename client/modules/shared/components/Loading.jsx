import React from 'react';

class Loading extends React.Component {

  loadingRender() {
    let props = this.props;
    return (
      <div className="hero section-loading">
        <div className="hero-body">
          <div className="container has-text-centered">
            <svg width={props.width} height={props.height} viewBox="0 0 187.3 93.7" preserveAspectRatio="xMidYMid meet">
              <path stroke={props.color} id="outline" fill="none" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10"
                    d="M93.9,46.4c9.3,9.5,13.8,17.9,23.5,17.9s17.5-7.8,17.5-17.5s-7.8-17.6-17.5-17.5c-9.7,0.1-13.3,7.2-22.1,17.1 				c-8.9,8.8-15.7,17.9-25.4,17.9s-17.5-7.8-17.5-17.5s7.8-17.5,17.5-17.5S86.2,38.6,93.9,46.4z" />
              <path id="outline-bg" opacity="0.05" fill="none" stroke={props.color} stroke-width="4" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="M93.9,46.4c9.3,9.5,13.8,17.9,23.5,17.9s17.5-7.8,17.5-17.5s-7.8-17.6-17.5-17.5c-9.7,0.1-13.3,7.2-22.1,17.1 				c-8.9,8.8-15.7,17.9-25.4,17.9s-17.5-7.8-17.5-17.5s7.8-17.5,17.5-17.5S86.2,38.6,93.9,46.4z" />
            </svg>
            <p className="is-uppercase loading-text">loading</p>
          </div>
        </div>
      </div>
    );
  }

  render() {
    return this.loadingRender();
  }

}

Loading.propTypes = {
  height: React.PropTypes.number,
  width: React.PropTypes.number,
  color: React.PropTypes.string
};

Loading.defaultProps = {
  height: 200,
  width: 300,
  color: '#EDEDED'
};

export default Loading;
