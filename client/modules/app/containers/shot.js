import Shot from '../components/ShotDetail/ShotDetail.jsx';
import {useDeps, composeWithTracker, composeAll} from 'mantra-core';

export const composer = ({context, shotId}, onData) => {
  const {Meteor, Collections} = context();

  if (Meteor.subscribe('shot.single', shotId).ready()) {
    const shot = Collections.Shots.findOne(shotId);
    onData(null, {shot});
  } else {
    const shot = Collections.Shots.findOne(shotId);
    if (shot) {
      onData(null, {shot});
    } else {
      onData();
    }
  }
};

export default composeAll(
  composeWithTracker(composer),
  useDeps()
)(Shot);
