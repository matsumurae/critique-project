import ShotList from '../components/ShotList/ShotList.jsx';
import {useDeps, composeWithTracker, composeAll} from 'mantra-core';

export const composer = ({context}, onData) => {
  const {Meteor, Collections} = context();
  if (Meteor.subscribe('shots.list').ready()) {
    const shots = Collections.Shots.find().fetch();
    onData(null, {shots});
  }
};

export default composeAll(
  composeWithTracker(composer),
  useDeps()
)(ShotList);
