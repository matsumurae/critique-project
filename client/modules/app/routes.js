import React from 'react';
import {mount} from 'react-mounter';
import {Accounts} from 'meteor/accounts-base';

import {
  AuthCheck,
  LayoutDefault,
  Simplest,
  NotFound,
} from '/client/configs/components.js';

import Register from './components/AccountRegister/Wrapper.jsx';
import Login from './components/AccountLogin/Wrapper.jsx';
import Password from './components/AccountPassword/Wrapper.jsx';
import Profile from './components/AccountProfile/Wrapper.jsx';
import Account from './components/AccountAccount/Wrapper.jsx';
import ShotList from './containers/shotlist.js';
import Shot from './components/ShotDetail/ShotDetail.jsx';

export default function (injectDeps, {FlowRouter}) {

  const AuthCheckCtx = injectDeps(AuthCheck);

  // Home page
  FlowRouter.route('/', {
    name: 'app.home',
    action() {
      mount(AuthCheckCtx, {
        LayoutDefault, content: () => (<Simplest title="App main screen"/>)
      });
    }
  });

  // 404 page
  FlowRouter.notFound = {
    action() {
      mount(AuthCheckCtx, {
        LayoutDefault, content: () => (<NotFound />)
      });
    }
  };

  // Shots list
  FlowRouter.route('/shots', {
    name: 'shots.list',
    action() {
      mount(AuthCheckCtx, {
        LayoutDefault, content: () => (<ShotList />)
      });
    }
  });

  // Shot detail
  FlowRouter.route('/shot/:shotId', {
    name: 'shot.single',
    action({shotId}) {
      mount(AuthCheckCtx, {
        LayoutDefault, content: () => (<Shot shotId={shotId} />)
      });
    }
  });

  // Reviews list
  FlowRouter.route('/reviews', {
    name: 'app.reviews',
    action() {
      mount(AuthCheckCtx, {
        LayoutDefault, content: () => (<Simplest title="Review list"/>)
      });
    }
  });

  // Users list
  FlowRouter.route('/users', {
    name: 'app.users',
    action() {
      mount(AuthCheckCtx, {
        LayoutDefault, content: () => (<Simplest title="User list"/>)
      });
    }
  });

  // Register page
  FlowRouter.route('/register', {
    name: 'app.register',
    action() {
      mount(AuthCheckCtx, {
        LayoutDefault, content: () => (<Register />),
        requireNotLoggedIn: true
      });
    }
  });

  // Login page
  FlowRouter.route('/login', {
    name: 'app.login',
    action() {
      mount(AuthCheckCtx, {
        LayoutDefault, content: () => (<Login />),
        requireNotLoggedIn: true
      });
    }
  });

  // Logout route
  FlowRouter.route('/logout', {
    name: 'app.logout',
    action() {
      Accounts.logout();
      // Meteor.logout(() => {
      FlowRouter.go('/login');
      // });
    }
  });

  // Reset password page
  FlowRouter.route('/password', {
    name: 'app.password',
    action() {
      mount(AuthCheckCtx, {
        LayoutDefault, content: () => (<Password />)
      });
    }
  });

  // Account page
  FlowRouter.route('/account', {
    name: 'app.account',
    action() {
      mount(AuthCheckCtx, {
        LayoutDefault, content: () => (<Account />),
        requireUserId: true
      });
    }
  });

  // Profile page
  FlowRouter.route('/profile', {
    name: 'app.profile',
    action() {
      mount(AuthCheckCtx, {
        LayoutDefault, content: () => (<Profile />),
        requireUserId: true
      });
    }
  });

}
