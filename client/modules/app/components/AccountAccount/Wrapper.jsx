import React from 'react';

// import dataComposer from '../../lib/AccountRegister.js';
import AccountComponent from './Component.jsx';
// const ComponentCtx = dataComposer(AccountComponent);

export default class extends React.Component {

  render() {
    return (
      <div>
        <h2>Your account data</h2>
        <AccountComponent />
      </div>
    );
  }
}
