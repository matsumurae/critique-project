import React from 'react';

export default class AccountComponent extends React.Component {

  checkEmail() {
    if (Meteor.user().emails != undefined) {
      return (
        <p>Your email: {Meteor.user().emails[0].address}</p>
      );
    }
    return (
      <p>Your email: EMTPY</p>
    );
  }

  render() {
    return this.checkEmail();
  }
}
