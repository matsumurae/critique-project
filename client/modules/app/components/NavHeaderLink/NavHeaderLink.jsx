import React from 'react';

class NavLink extends React.Component {

  isActive() {
    return FlowRouter.current().path === this.props.href;
  }

  linkRender() {
    let style = 'nav-item';
    let props = this.props;
    if (this.isActive()) {
      style += ' is-active';
    }
    return (
      <a className={style} href={props.href}>{props.name}</a>
    );
  }

  render() {
    return this.linkRender();
  }

}

NavLink.propTypes = {
  href: React.PropTypes.string.isRequired,
  name: React.PropTypes.string.isRequired
};

export default NavLink;
