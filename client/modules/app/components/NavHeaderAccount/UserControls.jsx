import React from 'react';
import { Button } from 'bulma-react';

import Avatar from '../../../shared/components/Avatar.jsx';

export default class extends React.Component {

  displayUser() {

    let props = this.props;

    return (
      <div className="nav-item">
        <Button is-transparent is-medium is-primary mr-10>Upload</Button>
        <Avatar {...props} />
        <Button to="/logout" is-transparent is-medium>Logout</Button>
      </div>
    );
  }

  displayGuest() {
    return (
      <div className="nav-item">
        <Button is-accent is-medium mr-10 to="/register">Sign up</Button>
        <Button is-transparent is-medium is-primary to="/login">Log in</Button>
      </div>
    );
  }

  render() {
    const {
      userId
    } = this.props;

    return userId ? this.displayUser() : this.displayGuest();
  }
}
