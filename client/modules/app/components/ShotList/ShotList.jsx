import React from 'react';

import Image from '../../../shared/components/Image.jsx';
import Tabs from '../../../shared/components/Tabs.jsx';

const ShotList = ({shots}) => (
  <div>
    <Tabs />
    <div className="gallery">
      {shots.map((shot, index) => (
        <Image key={index} imgSrc={shot.src} imgAlt={shot.title} href={`/shot/${shot.url}`} />
      ))}
    </div>
  </div>
);

export default ShotList;
