import React from 'react';

import {navHeaderComposer} from '../../../../configs/composers.js';

import Logo from '../../../shared/components/Logo.jsx';
import SearchBar from '../../../shared/components/SearchBar.jsx';
import NavHeaderAccount from '../NavHeaderAccount/NavHeaderAccount.jsx';
import NavLink from '../NavHeaderLink/NavHeaderLink.jsx';

const NavHeaderAccountCtx = navHeaderComposer(NavHeaderAccount);

export default class extends React.Component {

  render() {

    return (
      <header className="has-fixed-top">
        <nav className="nav nav-ebony shadow--2dp">
          <div className="nav-left">
            <a className="nav-logo" href="/">
              <Logo height={45} width={100} color="#EDF2F4" />
            </a>
            <NavLink href="/shots" name="Shots" />
            <NavLink href="/reviews" name="Reviews" />
            <NavLink href="/users" name="Users" />
          </div>
          <div className="nav-right">
            <NavHeaderAccountCtx />
            <SearchBar />
          </div>
        </nav>
      </header>
    );
  }
}
