import React from 'react';
import DropDown from './NavDropDown.jsx';

export default class extends React.Component {

  render() {
    const {
      userId
    } = this.props;

    var links4 = [];
    links4.push({url: '/admin', name: '/admin'} );
    links4.push({url: '/users', name: '/users'} );
    links4.push({url: '/users/add', name: '/users/add'} );
    let navAdmin = React.createElement(DropDown, {name: 'Admin', links: links4});

    var links5 = [];
    links5.push({url: '/profile', name: 'Profile'} );
    links5.push({url: '/logout', name: 'Logout'} );

    let navAccounts = React.createElement(DropDown, {name: 'Accounts', links: links5});

    return (
      <ul className="nav navbar-nav">

        <li className="">
          <a aria-expanded="false" role="button" href="/shots" target="">Shots</a>
        </li>

        <li className="">
          <a aria-expanded="false" role="button" href="/reviews" target="">Reviews</a>
        </li>

        { navAccounts }

        { userId ? navAdmin : null }

      </ul>
    );
  }

}
