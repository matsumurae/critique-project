import React from 'react';

import NavHeader from '../NavHeader/NavHeader.jsx';
import Footer from '../Footer/FooterSmall.jsx';

export default class extends React.Component {

  displayLayout() {
    return (
      <div>
        <NavHeader />
        <div className="gridd">
          {this.props.content()}
        </div>
        <Footer />
      </div>
    );
  }

  displayNotFound() {
    return (
      <div>
        {this.props.content()}
      </div>
    );
  }

  render() {

    if (FlowRouter.getRouteName() !== undefined) {
      return this.displayLayout();
    }
    return this.displayNotFound();
  }
}
