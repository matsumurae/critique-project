import React from 'react';

export default class extends React.Component {

  render() {
    return (
      <div className="footer-small has-fixed-bottom">
        <div className="columns">
          <div className="column has-text-left">
            <p className="m-0">&copy; 2016 Critique</p>
          </div>
          <div className="column has-text-centered">
            <a className="footer-link" href="#">About</a>
            <a className="footer-link" href="#">Help</a>
            <a className="footer-link" href="#">Contact</a>
            <a className="footer-link" href="#">Terms</a>
          </div>
          <div className="column has-text-right">
            <a className="material-icons font-18 footer-social" href="#">face</a>
            <a className="material-icons font-18 footer-social" href="#">accessibility</a>
            <a className="material-icons font-18 footer-social" href="#">favorite</a>
            <a className="material-icons font-18 footer-social" href="#">grade</a>
          </div>
        </div>
      </div>
    );
  }

}
