import React from 'react';
import { Button } from 'bulma-react';

export default class extends React.Component {

  render() {
    return (
      <div className="hero is-fullheight error__page has-text-centered">
        <div className="hero-body">
          <div className="container">
            <h1 className="error__heading">404</h1>
            <h2 className="error__title">Page not found</h2>
            <h3 className="error__text">It looks like you're lost.</h3>
          </div>
        </div>
        <div className="hero-foot">
          <div className="container__buttons">
            <Button is-white is-outlined mb-5 to="/">Return to homepage</Button>
            <a href="#" className="report-link">report link?</a>
          </div>
        </div>
      </div>
    );
  }

}
