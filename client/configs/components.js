import LayoutDefault from '/client/modules/app/components/Layout/Layout.jsx';
import AuthCheck from '/client/modules/app/components/AuthCheck/Wrapper.jsx';
import Simplest from '/client/modules/app/components/Simplest.jsx';
import NotFound from '/client/modules/app/components/NotFound/Wrapper.jsx';

export {
  AuthCheck,
  LayoutDefault,
  Simplest,
  NotFound
};
